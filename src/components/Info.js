import { Component } from "react";

class Info extends Component {
    render() { 
        let {firstName, lastName, favoriteNumber} = this.props;
        return (
            <p>My name is {lastName} {firstName} and my favourite number is {favoriteNumber}. 
            </p>
        )
    }
}

export default Info;