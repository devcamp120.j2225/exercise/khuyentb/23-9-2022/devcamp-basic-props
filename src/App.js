import Info from "./components/Info";

function App() {
  return (
    <div className="App">
      <Info firstName="Khuyen" lastName="Trinh Bao" favoriteNumber={10} ></Info>
    </div>
  );
}

export default App;
